<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{secure_asset('/css/app.css')}}" rel="stylesheet" type="text/css">

    </head>
    <body class="bg-primary">
        <div class="flex-center position-ref full-height">
            <div class="content">
                @yield('conteudo')                
            </div>
        </div>
        <br>
        <script type="text/javascript" src="{{secure_asset('/js/app.js')}}"></script>
    </body>
</html>