@extends('layout')

@section('conteudo')

<h1 align="center"> Cadastro de pais</h1>

<form action="{{route('cadastro-pais')}}" method="POST">
    {{ csrf_field() }}
    
    <div class="col-sm-10 col-md-5">
        <div class="form-group">
            <label for="nome" class="hidden-sm hidden-xs">Nome: </label>
            <input type="text" id="nome" name="nome" value="" class="form-control" placeholder="Nome" />
        </div>
        
        <hr />
        <div class="form-group">
            <label for="sigla" class="hidden-xs hidden-sm">Sigla: </label>
            <input type="text" name="sigla" value="" class="form-control" placeholder="Sigla" />
        </div>
    
        <hr />
        <div class="form-group">
            <button type="submit" value="Cadastrar" class="btn btn-success">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Cadastrar
            </button>
        </div>
    </div>
</form>



@endsection

