<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Pais;

class PaisController extends Controller
{
    public function listaPais (){
        $paises = \App\Pais::all();
        return view('pais.lista', ['paises' => $paises]);
    }
    
    public function exibirCadastro (){
        return view('pais.cadastrar');
    }
    
    public function cadastrar (){
    
        $pais = new Pais();
        $pais->nome = Input::get('nome');
        $pais->sigla = Input::get('sigla');
        $pais->save();
        
        return redirect(route('lista-pais'));
    }
}
