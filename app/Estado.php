<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estado';
    
    protected $primaryKey = 'idestado';
    
    public function pais(){
        return $this->belongsTo('App\Pais', 'idpais', 'idpais', 'pais');
    }
    
    public function cidades(){
        return $this->hasMany('App\Cidade', 'idestado', 'idestado', 'cidade');
    }
}
