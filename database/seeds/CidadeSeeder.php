<?php

use Illuminate\Database\Seeder;

class CidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paises =array (
            ['nome' => 'Brasil', 'sigla' => 'BRA'],
            ['nome' => 'Alemanha', 'sigla' => 'ALE']
        );
        DB::table('pais')->insert($paises);
        $this->command->info('Cadastrando paises!');

        $estados =array (
            ['idpais' => '1', 'nome' => 'Rio Grande do Sul', 'sigla' => 'RS', 'regiao' => 'SUL'],
            ['idpais' => '1', 'nome' => 'Parana', 'sigla' => 'PR', 'regiao' => 'SUL']
        );
        DB::table('estado')->insert($estados);
        $this->command->info('Cadastrando estados!');

        $cidades =array (
            ['idestado' => '1', 'nome' => 'Novo Hamburgo'],
            ['idestado' => '1', 'nome' => 'Igrejinha'],
            ['idestado' => '2', 'nome' => 'Pato Branco'],
            ['idestado' => '2', 'nome' => 'Curitiba'],
            ['idestado' => '2', 'nome' => 'Foz do Iguaçu']
        );
        DB::table('cidade')->insert($cidades);
        $this->command->info('Cadastrando cidades!');
    }
}
