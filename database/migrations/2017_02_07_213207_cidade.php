<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cidade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidade', function(Blueprint $table)
		{
		    $table->engine = 'InnoDB';
			$table->increments('idcidade', true);
			$table->integer('idestado')->unsigned();
			$table->string('nome');
			$table->timestamps();
		});
		
		Schema::table('cidade', function(Blueprint $table)
		{
		    $table->engine = 'InnoDB';
		    $table->foreign('idestado')->references('idestado')->on('estado')->onDelete('cascade')->onUpdate('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cidade');
    }
}
